import java.util.Scanner; 

public class LabProgram {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in); 
      int highwayNumber;
      int primaryNumber;

      highwayNumber = scnr.nextInt();
      
    int serviceNumber;
      serviceNumber = (highwayNumber % 100);

    if (highwayNumber > 999 || highwayNumber < 1 || highwayNumber == 200)
        System.out.println (highwayNumber +
			    " is not a valid interstate highway number.");
    else if (highwayNumber % 2 == 0 && highwayNumber < 100)
      {
	System.out.println ("I-" + highwayNumber +
			    " is primary, going east/west.");
      }
    else if (highwayNumber < 100)
      {
	System.out.println ("I-" + highwayNumber +
			    " is primary, going north/south.");
      }
    else if (highwayNumber % 2 == 0 && highwayNumber > 100)
      {
	System.out.println ("I-" + highwayNumber +
			    " is auxiliary, " + "serving I-" + serviceNumber +
			    ", going east/west.");
      }
    else if (highwayNumber > 100)
      {
	System.out.println ("I-" + highwayNumber +
			    " is auxiliary, " + "serving I-" + serviceNumber +
			    ", going north/south.");
      }

   }
}
